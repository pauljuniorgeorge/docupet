<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use MathPHP\Statistics\Descriptive;
use MathPHP\Statistics\Average;
use App\Models\Filter;

class ClassifyController extends BaseController
{

    public function classify(Request $request){

        $data = explode(",",$request->input("data"));
        $deviation = floatval($request->input("deviation"));

        foreach($data as $number){
            if(!is_numeric($number)){
                return response()->json(['error' => 'You can only enter numbers.']);
            }
        }

        if(count($data)<10){
            return response()->json(['error' => 'Enter atleast 10 numbers seperated by commas.']);
        }

        $sD = Descriptive::standardDeviation($data);

        $mean = Average::mean($data);
        $upperBoundary = $mean + ($deviation*$sD);
        $lowerBoundary = $mean - ($deviation*$sD);
        $n = count($data);

        foreach($data as $value){
            if($value > $lowerBoundary && $value < $upperBoundary)
            {
                $classification['Medium'][] = $value;
            }
            else if($value < $lowerBoundary){
                $classification['Low'][] = $value;
            }
            else{
                $classification['High'][] = $value;
            }
        }

        $classification['frequency'] = Filter::frequency($classification);
        $classification['equalWidths'] = Filter::equalWidths($classification);

        return response()->json($classification);
    }

}
