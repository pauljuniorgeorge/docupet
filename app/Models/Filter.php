<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;

class Filter extends Model
{
    use HasFactory;

   public static function equalWidths($classification){
       $difference = max($classification['High']) - min($classification['High']);
       if($difference == max($classification['Medium']) - min($classification['Medium']))
       {
           if($difference == max($classification['Low']) - min($classification['Low']) ){
              return true;
           }
        }
       return false;
   }

    public static function frequency($classification){

        if(count($classification['High']) == count($classification['Medium'])){
            if(count($classification['Medium']) == count($classification['Low'])){
                return true;
            }
        }
        return false;
    }
}
