<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;
use App\Models\Filter;

class FilterTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testEqualWidths()
    {
        $classification["Low"] = [0,10];
        $classification["Medium"] = [10,20];
        $classification["High"] = [20,30];
        $this->assertEquals(true,Filter::equalWidths($classification));
    }

    public function testEqualWidthsMultipleNumberDifference()
    {
        $classification["Low"] = [0,5,3,10];
        $classification["Medium"] = [10,13,20];
        $classification["High"] = [20,21,22,23,24,25,26,27,28,29,30];
        $this->assertEquals(true,Filter::equalWidths($classification));
    }

    public function testEqualWidthsNegative()
    {
        $classification["Low"] = [0,10];
        $classification["Medium"] = [14,22];
        $classification["High"] = [24,35];
        $this->assertEquals(false,Filter::equalWidths($classification));
    }

    public function testEqualWidthsMultipleNumberDifferenceNegative()
    {
        $classification["Low"] = [0,5,3,11];
        $classification["Medium"] = [10,13,21];
        $classification["High"] = [20,21,22,23,24,25,26,27,28,29,52];
        $this->assertEquals(false,Filter::equalWidths($classification));
    }

    public function testFrequency()
    {
        $classification["Low"] = [0,5,3,1];
        $classification["Medium"] = [10,13,14,15];
        $classification["High"] = [20,21,22,23];
        $this->assertEquals(true,Filter::frequency($classification));
    }

    public function testFrequencyNegative()
    {
        $classification["Low"] = [0,5,3,1];
        $classification["Medium"] = [10,13,20];
        $classification["High"] = [22,23];
        $this->assertEquals(false,Filter::frequency($classification));
    }



}
