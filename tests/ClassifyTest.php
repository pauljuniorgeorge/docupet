<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class ClassifyTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testReversion()
    {
        // Working Case
        $this->post('/classify' ,['data' => '0.1, 3.4, 3.5, 3.6, 7.0, 9.0, 6.0, 4.4, 2.5, 3.9, 4.5, 2.8','deviation' => 1])
        ->seeJsonEquals(["Low"=>["0.1"],"Medium"=>[" 3.4"," 3.5"," 3.6"," 6.0"," 4.4"," 2.5"," 3.9"," 4.5"," 2.8"],"High"=>[" 7.0"," 9.0"],"frequency"=>false,"equalWidths"=>false]);
    }

    public function testNoNumbers()
    {
        $this->post('/classify' ,['data' => ''])
            ->seeJsonContains(["error"=>"You can only enter numbers."]);
    }

    public function testMixedNumbers()
    {
        $this->post('/classify' ,['data' => '0.1, 3.4, 3.5, 3.6, 7.0, apple, "oranges", 4.4, 2.5, 3.9, 4.5,'])
            ->seeJsonContains(["error"=>"You can only enter numbers."]);
    }

    public function testLessThanTenNumbers()
    {
        $this->post('/classify' ,['data' => '0.1, 3.4, 3.5, 3.6, 7.0,2.5, 3.9, 4.5'])
            ->seeJsonContains(["error"=>"Enter atleast 10 numbers seperated by commas."]);
    }


}
