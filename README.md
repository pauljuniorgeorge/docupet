# Docupet Assignment

This test was completed on Ubuntu 20.04 using Apache.

## Usage

Afer configuring a virtual host make a POST request to the /classify endpoint.
This endpoint takes two parameters.

Data => A String of numbers seperated by commas

Deviation => How many Standard Deviations about the mean you wish to consider Medium.

## Examples 
This example is from the tests suite and demonstrates how to make a request

$this->post('/classify' ,

[

'data' => '0.1, 3.4, 3.5, 3.6, 7.0, 9.0, 6.0, 4.4, 2.5, 3.9, 4.5, 2.8',

'deviation' => 1

]);

## Response 
An Example response is show below

{

"Low":["0.1"],

"Medium":[" 3.4"," 3.5"," 3.6"," 6.0"," 4.4"," 2.5"," 3.9"," 4.5"," 2.8"],

"High":[" 7.0"," 9.0"],

"frequency":false,

"equalWidths":false

}
